package algorithm;

import java.util.List;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * Class intended to count time according to simulation speed. By default
 * simulation speed is set to be in real time. This class is singleton. 
 * 
 * @author Adam Świeżowski
 *
 */
public class ModelTime implements Runnable {
	private static ModelTime instance = null;
	private List<ChangeListener> chEventListeners = new Vector<ChangeListener>(
			2);
	private int days, hours, minutes;

	/**
	 * The interval in milisecs to travel 7.5m at 10km/h this represents time to
	 * real world
	 */
	final public long realSpeed = 2700;

	private long speed = 2700;

	/**
	 * Proportion of speed to real world time
	 */
	private double timeFactor = 1.;
	private double secDur = 1.;

	/**
	 * time in seconds
	 */
	private volatile double time = 0;
	
	private volatile boolean runnig=true;
	
	private void ModelTime() {
	}

	public static ModelTime getInstance() {
		if (instance == null) {
			instance = new ModelTime();
		}
		return instance;
	}

	/**
	 * Set refresh interval in milliseconds
	 * 
	 * @param speed
	 *            is interval in ms
	 */

	public void setSpeed(int speed) {
		this.speed = speed;
		timeFactor = (double) speed / realSpeed;
		secDur = realSpeed / (double) speed;
	}

	public long getSpeed() {
		return speed;
	}

	public int getDays() {
		return (int) time / 86400;
	}

	public int getHours() {
		return (int) (time % 86400) / 3600;
	}

	public int getMinutes() {
		return (int) ((time % 86400) % 3600 / 60);
	}

	public int getTotalHours() {
		return (int) time / 3600;
	}

	public int getTotalMinutes() {
		return (int) time / 60;
	}

	public int getTotalTime() {
		return (int) time;
	}

	/**
	 * Start time counting
	 */
	public void run() {
		int minutes;
		int hours;
		int days;
		boolean changed = false;
		
		while (runnig) {
			try {
				TimeUnit.MILLISECONDS.sleep((long) (1000 * timeFactor));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			time += 1.;
			days = (int) (time / 86400);
			hours = (int) (time % 86400 / 3600);
			minutes = (int) (time % 3600 / 60);
			if (minutes != this.minutes) {
				this.minutes = minutes;
				changed = true;
			}
			if (hours != this.hours) {
				this.hours = hours;
				changed = true;
			}
			if (days != this.days) {
				this.days = days;
				changed = true;
			}
			if (changed) {
				fireChangeEvent();
				changed = false;
			}
		}

	}

	private void fireChangeEvent() {
		ChangeEvent e = new ChangeEvent(this);
		for (ChangeListener l : chEventListeners) {
			l.stateChanged(e);
		}
	}

	public void removeChangeEventListener(ChangeListener e) {
		chEventListeners.remove(e);
	}

	public void addChangeEvent(ChangeListener e) {
		chEventListeners.add(e);
	}

	public double getTimeFactor() {
		return secDur;
	}
	
	public void resetTime(){
		time=0.;
	}
	
	public void stop(){
		runnig=false;
	}
	
	public void start(){
		runnig=true;
		Thread timeTread=new Thread(this);
		timeTread.start();
	}
}
