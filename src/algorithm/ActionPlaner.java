package algorithm;

import java.util.Iterator;
import java.util.Random;
import java.util.Vector;

import city.Action;
import city.Car;
import city.StreetBlock;

/**
 * Class ActionPlaner calculates next route for Car.
 * 
 * @author Adam Świeżowski
 *
 */

public class ActionPlaner {

	private Random rand = new Random();
	final private double probability = 0.1;
	final private double turnProbability = 0.8;
	private Vector<OSMNode> lockedNodes = new Vector<OSMNode>(50);
	private Vector<StreetBlock> lockedBlocks = new Vector<StreetBlock>(500);

	public class EndOfRoadException extends Exception {
		private static final long serialVersionUID = 1L;
	}

	public class BlockedRoadException extends Exception {
		private static final long serialVersionUID = -91663395000912692L;
	};

	public ActionPlaner() {
		rand = new Random();
	}

	/**
	 * Function finds next way by random with probability. Function don't allow
	 * to turn back and checks if road is drivable
	 * 
	 * @param blocks
	 *            from which the road will be chosen
	 * @param currentBlock
	 *            occupied by car
	 * @return next empty block
	 * @throws EndOfRoadException
	 * @throws BlockedRoadException
	 */
	private StreetBlock getRandBlock(Vector<StreetBlock> blocks,
			StreetBlock currentBlock) throws EndOfRoadException,
			BlockedRoadException {
		int probabilitySum = 0;
		StreetBlock nextBlock = null;
		/*
		 * If there is no way to go throw exception
		 */
		if (blocks.size() < 2) {
			throw new EndOfRoadException();
		}
		/*
		 * If there is only one way choose way forward and check if can move to
		 * that block
		 */
		else if (blocks.size() == 2) {
			// Check if not going backward
			if (blocks.get(1).getForward() == null
					&& blocks.get(0).getForward() == null) {
				throw new EndOfRoadException();
			}
			if (blocks.get(0) == currentBlock
					&& currentBlock.getBackwardNode() != blocks.get(0)
							.getNextNode()) {
				if (!blocks.get(1).isEmpty() || blocks.get(1).isLocked()
						|| blocks.get(1).getForward() == null)
					throw new BlockedRoadException();
				return blocks.get(1).getForward();
			} else if (currentBlock.getBackwardNode() != blocks.get(1)
					.getNextNode()) {
				if (!blocks.get(0).isEmpty() || blocks.get(0).isLocked()
						|| blocks.get(0).getForward() == null)
					throw new BlockedRoadException();
				return blocks.get(0).getForward();
			} else {
				throw new BlockedRoadException();
			}
		}

		Vector<StreetBlock> nextWays = new Vector<StreetBlock>(3);
		for (StreetBlock s : blocks) {
			if (s.getForward() != null && s.getNextNode() != null
					&& s.getNextNode() != currentBlock.getBackwardNode()) {
				nextWays.add(s);
			}
		}

		if (nextWays.size() == 0) {
			throw new EndOfRoadException();
		}

		for (StreetBlock s : nextWays) {
			probabilitySum += s.getNextNode().getProbability();
		}

		int probabilityIndex = rand.nextInt(probabilitySum);
		int index = 0;
		probabilitySum = 0;
		while (probabilitySum <= probabilityIndex) {
			probabilitySum += nextWays.get(index).getNextNode()
					.getProbability();
			index++;
		}
		nextBlock = nextWays.get(index - 1);

		if (!nextBlock.isEmpty() || nextBlock.isLocked())
			throw new BlockedRoadException();
		return nextBlock;
	}

	static public boolean isEmpty(OSMNode node) {
		if (node == null)
			return true;
		if (node.isLocked())
			return false;
		for (StreetBlock s : node.getStreetBlocks()) {
			if (s.isLocked()) {
				return false;
			}
		}
		return true;
	}

	private void addBlockLock(StreetBlock b, Car c) {
		b.changed();
		b.notifyObservers(c);
		b.lock();
		lockedBlocks.add(b);
	}

	/**
	 * Function get Car and set Next Action in it. It sets next position and
	 * speed of the Car
	 * 
	 * @param Car
	 * @throws EndOfRoadException
	 */
	public void setNextAction(Car car) throws EndOfRoadException {

		StreetBlock pos = car.getStreetBlock();
		StreetBlock nextPos = pos;
		Integer carSpeed = car.getSpeed();
		Action action = car.getAction();
		// 1st step
		if (carSpeed < pos.getMaxSpeed()) {
			carSpeed += 1;
		}
		// 3rd step
		if (rand.nextDouble() < probability && carSpeed < 0) {
			carSpeed -= 1;
		}

		addBlockLock(pos, car);
		// 2nd step
		for (int i = 0; i < carSpeed; i++) {
			if (nextPos.getForward() == null) {
				OSMNode nd = nextPos.node;
				nd.lock();
				lockedNodes.add(nd);
				try {
					nextPos = getRandBlock(nd.getStreetBlocks(), nextPos);
					addBlockLock(nextPos, car);
				} catch (BlockedRoadException e) {
					carSpeed = 0;
					break;
				}
				i++;
			} else if (nextPos.getForward().node != null) {
				OSMNode nd = nextPos.getForward().node;
				nd.lock();
				lockedNodes.add(nd);
				try {
					nextPos = getRandBlock(nd.getStreetBlocks(),
							nextPos.getForward());
					addBlockLock(nextPos, car);
				} catch (BlockedRoadException e) {
					carSpeed = 0;
					break;
				}
				i++;
			} else if (!nextPos.getForward().isLocked()) {
				nextPos = nextPos.getForward();
				addBlockLock(nextPos, car);
			} else if (nextPos.getForward().getLeft() != null
					&& !nextPos.getForward().getLeft().isLocked()
					&& !nextPos.getLeft().isLocked()
					&& rand.nextDouble() < turnProbability) {
				nextPos = nextPos.getForward().getLeft();
				addBlockLock(nextPos, car);
			} else if (nextPos.getForward().getRight() != null
					&& !nextPos.getForward().getRight().isLocked()
					&& !nextPos.getRight().isLocked()
					&& rand.nextDouble() < turnProbability) {
				nextPos = nextPos.getForward().getRight();
				addBlockLock(nextPos, car);
			} else {
				if (nextPos.getForward().isLocked()) {
					carSpeed = 0;
				} else {
					carSpeed = i;
				}
				break;
			}

		}
		action.setSpeed(carSpeed);
		action.setNextBlock(nextPos);

	}

	/**
	 * Unlock blocked nodes - nodes through which Car went in current iteration
	 */
	public void unlockNodes() {
		Iterator<OSMNode> i = lockedNodes.iterator();

		while (i.hasNext()) {
			i.next().unlock();
			i.remove();
		}
	}

	/**
	 * Unlock blocked StreetBlock - blocks through which Car went in current
	 * iteration
	 */
	public void unlockStreetBlocks() {
		Iterator<StreetBlock> i = lockedBlocks.iterator();

		while (i.hasNext()) {
			i.next().unlock();
			i.remove();
		}
	}
}
