package algorithm;

import java.util.Vector;

import city.StreetBlock;

/**
 * OSMNode represents geographical position of points from osm file. Nodes are
 * used to create routes between them
 * 
 * @see <a href="http://wiki.openstreetmap.org/wiki/OSM_XML">OSM XML</a>
 * @author Adam Świeżowski
 *
 */

public class OSMNode {
	private Double latitude, longitude;
	private Long id;
	private Vector<StreetBlock> blocks;
	private boolean trafficSignals = false;
	private long sumSpeed;
	private int carCount;
	private int probability;
	private boolean locked = false;
	static int count = 0;
	// radious of Earth
	static final Double R = 6371009.;

	/**
	 * Creates node with ID=0 and position 0, 0
	 */
	public OSMNode() {
		this(0L, 0, 0);
	}

	public OSMNode(Long id, double latitude, double longtitude) {
		count++;
		blocks = new Vector<StreetBlock>(2);
		this.id = id;
		this.latitude = latitude;
		this.longitude = longtitude;
		sumSpeed = 0;
		carCount = 0;
	}

	public long getId() {
		return this.id;
	}

	/**
	 * Set geographical position of Node
	 * 
	 * @param latitude
	 * @param longtitude
	 */
	public void setGeo(double latitude, double longtitude) {
		this.latitude = latitude;
		this.longitude = longtitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void addStreetBlock(StreetBlock block) {
		this.blocks.add(block);
	}

	public Vector<StreetBlock> getStreetBlocks() {
		return blocks;
	}

	public void setTrafficSignals() {
		trafficSignals = true;
	}

	public boolean hasTrafficSignals() {
		return trafficSignals;
	}

	/**
	 * Calculate distance in meters between two nodes. Function uses haversine
	 * formula.
	 * 
	 * @param node2
	 * @return distance beetween nodes in meters
	 * @see <a
	 *      href="http://www.movable-type.co.uk/scripts/latlong.html">Calculate
	 *      distance, bearing and more between Latitude/Longitude points</a>
	 */
	public Double getGeoDistance(OSMNode node2) {
		double dLat = Math.toRadians(node2.latitude - latitude);
		double dLng = Math.toRadians(node2.longitude - longitude);
		double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
				+ Math.cos(Math.toRadians(latitude))
				* Math.cos(Math.toRadians(node2.latitude)) * Math.sin(dLng / 2)
				* Math.sin(dLng / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double distance = R * c;

		return distance;
	}

	/**
	 * Calculate distance in meters between nodes. Faster and less accurate
	 * formula.
	 * 
	 * @param node2
	 * @return distance beetween nodes in meters
	 */
	public Double getDistance(OSMNode node2) {
		double a = (node2.latitude - latitude) * 111229.02643399208;
		double b = (node2.longitude - longitude) * 71695.72612886579;
		return Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
	}

	/**
	 * Calculate bearing nodes
	 * 
	 * @param node2
	 * @return degree in radians
	 * @see <a
	 *      href="http://www.movable-type.co.uk/scripts/latlong.html">Calculate
	 *      distance, bearing and more between Latitude/Longitude points</a>
	 */
	public Double getBearing(OSMNode node2) {
		double longitude1 = longitude;
		double longitude2 = node2.longitude;
		double latitude1 = Math.toRadians(latitude);
		double latitude2 = Math.toRadians(node2.latitude);
		double longDiff = Math.toRadians(longitude2 - longitude1);
		double y = Math.sin(longDiff) * Math.cos(latitude2);
		double x = Math.cos(latitude1) * Math.sin(latitude2)
				- Math.sin(latitude1) * Math.cos(latitude2)
				* Math.cos(longDiff);
		return Math.atan2(y, x);
	}

	/**
	 * Calculates the latitude of StreetBlock at given distance between nodes
	 * 
	 * @param node2
	 *            Second node from which latitude will be get
	 * @param number
	 *            the number of segment part
	 * @param count
	 *            count of blocks
	 * @return latitude of StreetBlock
	 */
	public Double getBlockLatitude(OSMNode node2, Integer number, Integer count) {
		return (number * (node2.latitude - latitude)) / (count + 1) + latitude;
	}

	/**
	 * Calculates the longitude of StreetBlock at given distance between nodes
	 * 
	 * @param node2
	 *            Second node from which latitude will be get
	 * @param number
	 *            the number of segment part
	 * @param count
	 *            count of blocks
	 * @return longitude of StreetBlock
	 */
	public Double getBlockLongitude(OSMNode node2, Integer number, Integer count) {
		return (number * (node2.longitude - longitude)) / (count + 1)
				+ longitude;
	}

	public void addSpeed(Integer speed) {
		sumSpeed += speed;
	}

	public long getSpeeds() {
		return sumSpeed;
	}

	public void addCarCount() {
		carCount += 1;
	}

	public int getCarCount() {
		return carCount;
	}

	static public Integer getCount() {
		return count;
	}

	public int getProbability() {
		return probability;
	}

	/**
	 * Set probability of generating Car in selected node and turning Car in the
	 * way of Node
	 * 
	 * @param probability
	 */
	public void setProbability(int probability) {
		this.probability = probability;
	}

	public void lock() {
		locked = true;
	}

	public void unlock() {
		locked = false;
	}

	public boolean isLocked() {
		return locked;
	}

	public String toString() {
		StringBuilder name = new StringBuilder();
		for (StreetBlock s : blocks) {
			if (s.getName() != null) {
				name.append(s.getName());
				name.append(",");
			}
		}
		if (name.length() == 0) {
			name.append("Id: ");
			name.append(id);
		}
		return name.toString();
	}

	public boolean isDrivable(OSMNode backward, OSMNode forward) {
		for (StreetBlock s : blocks) {
			if (s.getCar() != null) {
				if (s.getNextNode() != backward && s.getNextNode() != forward) {
					return false;
				}
			}
		}
		return true;
	}

}
