package algorithm;

public class Test {

	public static void main(String[] args) {
		OSMNode node1=new OSMNode();
		OSMNode node2=new OSMNode();
		
		node1.setGeo(50., 20.);
		node2.setGeo(50.0, 19.);
		System.out.println(node2.getGeoDistance(node1));
		System.out.println(Math.toDegrees(node1.getBearing(node2)));
		System.out.println(node2.getDistance(node1));
	}

}
