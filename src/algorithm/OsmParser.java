package algorithm;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import city.StreetBlock;
import city.TrafficLights;

import javax.xml.parsers.*;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Parses osm file and creates OSMNodes and StreetBlocks.
 * 
 * @author Adam Świeżowski
 */

public class OsmParser {

	private Map<Long, OSMNode> intToNode;
	private Document dom;
	final double blockLength = 7.5;
	private StreetBlock firstBlock = null;
	private StreetBlock lastBlock;
	final private double lineGeoWidth = .00007;

	public OsmParser() {
		intToNode = new HashMap<Long, OSMNode>();
	}

	/**
	 * Get list of loaded nodes
	 * @return list of OSMNodes
	 */
	public Collection<OSMNode> getNodes() {
		return intToNode.values();
	}

	/**
	 * Get HashMap of IDs to OSMNodes 
	 * @return IDs to OSMNodes HashMap
	 */
	public Map<Long, OSMNode> getHashMap() {
		return intToNode;
	}

	/**
	 * @return first created block
	 */
	public StreetBlock getFirstStreetBlock() {
		return firstBlock;
	}

	/**
	 * @return last created StreetBlock
	 */
	public StreetBlock getLastStreetBlock() {
		return lastBlock;
	}

	/**
	 * Start loading file
	 */
	public void execute() {
		loadFile();
		parseDocumentOSMNodes();
		parseDocumentOSMWays();
	}

	private void loadFile() {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = null;
		try {
			db = dbf.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			dom = db.parse("highways.osm");
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void parseDocumentOSMNodes() {
		Element docEle = dom.getDocumentElement();
		NodeList nl = docEle.getElementsByTagName("node");
		for (int i = 0; i < nl.getLength(); i++) {

			Element ele = (Element) nl.item(i);
			Long id = new Long(ele.getAttribute("id"));
			Double lat = new Double(ele.getAttribute("lat"));
			Double lon = new Double(ele.getAttribute("lon"));
			OSMNode n = new OSMNode(id, lat, lon);

			// Search for traffic signals
			NodeList nodeTags = docEle.getElementsByTagName("tag");
			for (int j = 0; j < nodeTags.getLength(); j++) {
				Element eleTag = (Element) nodeTags.item(i);
				if (eleTag != null) {
					if (eleTag.getAttribute("k").equals("highway")
							&& eleTag.getAttribute("v").equals(
									"traffic_signals")) {
						n.setTrafficSignals();
					}
				}
			}
			intToNode.put(id, n);
		}
	}

	private void parseDocumentOSMWays() {
		Element docEle = dom.getDocumentElement();
		NodeList nl = docEle.getElementsByTagName("way");
		for (int i = 0; i < nl.getLength(); i++) {
			Element ele = (Element) nl.item(i);
			addStreetBlocks(ele);
		}
	}

	private Double getXTransaltion(Double angle) {
		return Math.cos(angle) * lineGeoWidth;
	}

	private Double getYTransaltion(Double angle) {
		return Math.sin(angle) * lineGeoWidth;
	}

	private void addLane(StreetBlock right, double xTranslation,
			double yTranslation) {
		StreetBlock newBlock, previousBlock = null;
		while (right != null) {
			newBlock = new StreetBlock();
			newBlock.setMaxSpeed(right.getMaxSpeed());
			newBlock.setLatitude(right.getLatitude() + xTranslation);
			newBlock.setLongitude(right.getLongitude() - yTranslation);
			if (previousBlock != null) {
				previousBlock.setForward(newBlock);
			}
			newBlock.setBackward(previousBlock);
			newBlock.setAngle(right.getAngle());
			newBlock.node = right.node;
			newBlock.setNextNode(right.getNextNode());
			newBlock.setBackwardNode(right.getBackwardNode());

			right.setLeft(newBlock);
			newBlock.setRight(right);

			if(right.node!=null){
				right.node.addStreetBlock(newBlock);
			}
			
			previousBlock = newBlock;

			right = right.getForward();
		}
	}

	private void addBlocksBetweenNodes(OSMNode node1, OSMNode node2,
			Integer maxSpeed, Integer lanes, Boolean r, double angle,
			double xTranslation, double yTranslation, String name) {
		Double nodeDistance = node1.getDistance(node2);
		Integer blockCount = (int) (Math.round(nodeDistance / blockLength) - 2);

		StreetBlock previousBlock = null, node1Block = null;
		StreetBlock node2Block = null;

		Boolean junction = false;
		TrafficLights trafficLights = TrafficLights.NONE;
		if (node1.hasTrafficSignals())
			trafficLights = TrafficLights.GREEN;
		StreetBlock newBlock = new StreetBlock(node1.getLatitude()
				+ xTranslation, node1.getLongitude() - yTranslation, maxSpeed,
				trafficLights, junction, angle);

		node1.addStreetBlock(newBlock);
		newBlock.node = node1;
		if (firstBlock == null)
			firstBlock = newBlock;
		previousBlock = newBlock;

		node1Block = previousBlock;
		previousBlock.setName(name);
		previousBlock.setNextNode(node2);
		junction = false;
		trafficLights = TrafficLights.NONE;
		if (node2.hasTrafficSignals())
			trafficLights = TrafficLights.GREEN;
		newBlock = new StreetBlock(node2.getLatitude() + xTranslation,
				node2.getLongitude() - yTranslation, maxSpeed, trafficLights,
				junction, angle);
		if (node2.hasTrafficSignals())
			newBlock.setTrafficLights(TrafficLights.GREEN);
		node2.addStreetBlock(newBlock);
		newBlock.node = node2;
		node2Block = newBlock;

		node2Block.setBackwardNode(node1);

		for (int j = 0; j < blockCount; j++) {
			newBlock = new StreetBlock();
			newBlock.setMaxSpeed(maxSpeed);
			newBlock.setLatitude(node1.getBlockLatitude(node2, j + 1,
					blockCount) + xTranslation);
			newBlock.setLongitude(node1.getBlockLongitude(node2, j + 1,
					blockCount) - yTranslation);

			previousBlock.setForward(newBlock);
			newBlock.setBackward(previousBlock);
			newBlock.setAngle(angle);

			previousBlock = newBlock;
		}
		previousBlock.setForward(node2Block);
		node2Block.setBackward(previousBlock);
		lastBlock = node2Block;

		StreetBlock firstNodeBlock = node1Block;
		for (int i = 1; i < lanes; i++) {
			addLane(firstNodeBlock, -xTranslation, -yTranslation);
			firstNodeBlock = firstNodeBlock.getLeft();
		}
	}

	private void addStreetBlocks(Element wayElement) {
		Integer maxSpeed = 0;
		Integer lanes = 0;
		// range of probability is 0-100
		int probability = 10;
		Boolean oneway = false;
		String name = null;
		NodeList nl = wayElement.getElementsByTagName("tag");
		for (int i = 0; i < nl.getLength(); i++) {
			Element ele = (Element) nl.item(i);
			String attr = ele.getAttribute("k");
			if (attr.equals("lanes")) {
				lanes = new Integer(ele.getAttribute("v"));
			} else if (attr.equals("maxspeed")) {
				maxSpeed = Integer.parseInt(ele.getAttribute("v")) / 10;
			} else if (attr.equals("oneway")) {
				if (ele.getAttribute("v").equals("yes")) {
					oneway = true;
				} else {
					oneway = false;
				}
			} else if (attr.equals("highway")) {
				String val = ele.getAttribute("v");
				if (val.equals("motorway")) {
					probability = 1000;
				} else if (val.equals("trunk")) {
					probability = 1000;
				} else if (val.equals("primary")) {
					probability = 1000;
				} else if (val.equals("secondary")) {
					probability = 70;
				} else if (val.equals("tertiary")) {
					probability = 70;
				}
			} else if (attr.equals("name")) {
				name = ele.getAttribute("v");
			}
		}
		if (lanes == 0)
			lanes = 1;
		if (maxSpeed == 0)
			maxSpeed = 5;
		nl = wayElement.getElementsByTagName("nd");

		Long nodeId1 = null;
		Long nodeId2 = null;
		Double xTranslation = 0.;
		Double yTranslation = 0.;
		for (int i = 0; i < nl.getLength() - 1; i++) {
			Element ele1 = (Element) nl.item(i);
			Element ele2 = (Element) nl.item(i + 1);
			nodeId1 = Long.parseLong(ele1.getAttribute("ref"));
			nodeId2 = Long.parseLong(ele2.getAttribute("ref"));
			OSMNode node1 = intToNode.get(nodeId1);
			OSMNode node2 = intToNode.get(nodeId2);
			node1.setProbability(probability);
			node2.setProbability(probability);
			Double angle = Math.PI + node1.getBearing(node2);
			//if (!oneway) {
				xTranslation = getXTransaltion(angle);
				yTranslation = getYTransaltion(angle);
			//}
			addBlocksBetweenNodes(node1, node2, maxSpeed, lanes, true, angle,
					xTranslation, yTranslation, name);

			if (!oneway) {
				angle = Math.PI + angle;
				addBlocksBetweenNodes(node2, node1, maxSpeed, lanes, false,
						angle, -xTranslation, -yTranslation, name);
			}
		}
	}
}
