package algorithm;

import java.util.Collection;
import java.util.Iterator;
import java.util.Random;
import java.util.Vector;

import city.Car;
import city.StreetBlock;

/**
 * Generates random car with probability in random node
 * 
 * @author Adam Świeżowski
 */

public class CarGenerator {
	private Collection<OSMNode> nodes;
	private Random rand;
	private int totalProbabilitySum;

	public class NotEmptyBlock extends Exception {
		private static final long serialVersionUID = 2625405467207711910L;
	};

	public CarGenerator(Collection<OSMNode> collection) {
		rand = new Random();
		this.nodes = collection;
		calculateProbabilitySum();
	}

	private void calculateProbabilitySum() {
		totalProbabilitySum = 0;
		for (OSMNode n : nodes) {
			totalProbabilitySum += n.getProbability();
		}
	}

	/**
	 * Generates random Car with probability. If random block is occupied throw
	 * exception.
	 * 
	 * @return Car
	 * @throws NotEmptyBlock
	 * 
	 * @see {@link OSMNode#setProbability(int)}
	 */

	public Car getRandCar() throws NotEmptyBlock {
		int probabilityIndex = 0;
		int sum = 0;
		Iterator<OSMNode> nodeIterator = nodes.iterator();
		OSMNode node = null;
		Car newCar;
		probabilityIndex = rand.nextInt(totalProbabilitySum);
		sum = 0;
		while (sum <= probabilityIndex) {
			node = nodeIterator.next();
			sum += node.getProbability();

		}

		Vector<StreetBlock> blocks = node.getStreetBlocks();
		Vector<StreetBlock> goodBlocks = new Vector<StreetBlock>(3);

		for (StreetBlock pos : blocks) {
			if (pos.isEmpty() && !pos.isLocked() && pos.getForward() != null) {
				goodBlocks.add(pos);
			}
		}

		if (goodBlocks.size() == 0)
			throw new NotEmptyBlock();
		StreetBlock pos = goodBlocks.get(rand.nextInt(goodBlocks.size()));
		newCar = new Car();
		newCar.setStreetBlock(pos);
		newCar.setSpeed(rand.nextInt(pos.getMaxSpeed()));
		return newCar;
	}
}
