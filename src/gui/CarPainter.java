package gui;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.util.Iterator;
import java.util.Set;

import org.jdesktop.swingx.painter.Painter;
import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.mapviewer.GeoPosition;

import city.Car;
import city.StreetBlock;

/**
 * Paints car on map with appropriate tilt from StreetBlock
 * 
 * @author Adam Świeżowski
 */

public class CarPainter implements Painter<JXMapViewer> {

	private Set<Car> cars;
	private int carSize=2;
	private int[] xpoints=new int[3];
	private int[] ypoints=new int[3];
	private int[] translatedXpoints=new int[3];
	private int[] translatedYpoints=new int[3];
	private StreetBlock block;
	
	public void setBlock(StreetBlock block){
		this.block=block;
	}
	
	public void setCars(Set<Car> set) {
		this.cars = set;
	}

	public CarPainter() {
	}

	private int rotateX(int centerx,int centery, int pointx, int pointy, double angle){
		return (int) (centerx + (pointx-centerx)*Math.cos(angle) - (pointy-centery)*Math.sin(angle));
	}
	
	private int rotateY(int centerx,int centery, int pointx, int pointy, double angle){
		return (int) (centery + (pointx-centerx)*Math.sin(angle) + (pointy-centery)*Math.cos(angle));
	}
	
	public void paint(Graphics2D g, JXMapViewer viewer, int w, int h) {

		g = (Graphics2D) g.create();
		// convert from viewport to world bitmap
		Rectangle rect = viewer.getViewportBounds();
		g.translate(-rect.x, -rect.y);
		
		if(block!=null){
			GeoPosition geoPosition = new GeoPosition(block.getLatitude(),block.getLongitude());
			Point2D point = viewer.getTileFactory().geoToPixel(geoPosition,
					viewer.getZoom());
			g.setColor(Color.BLACK);
			int x = (int) point.getX();
			int y = (int) point.getY();
			g.fillRect(x-4, y-4, 8, 8);
		}
		
		synchronized (cars) {
			Iterator<Car> iter=cars.iterator();
			while (iter.hasNext()) {
				Car c=iter.next();
				GeoPosition geoPosition = new GeoPosition(c.getStreetBlock().getLatitude(),c.getStreetBlock().getLongitude());
				Point2D point = viewer.getTileFactory().geoToPixel(geoPosition,
						viewer.getZoom());
	
				int x = (int) point.getX();
				int y = (int) point.getY();
				if(c.getColor()!=null)
					g.setColor(c.getColor());
				else
					g.setColor(Color.GREEN);
				//g.fillRect(x, y, carSize, carSize);
				xpoints[0]=x-carSize/2; ypoints[0]=y-carSize/2;
				xpoints[1]=x+carSize/2; ypoints[1]=y-carSize/2;
				xpoints[2]=x;			ypoints[2]=y+carSize;
				translatedXpoints[0]=rotateX(x, y, xpoints[0], ypoints[0], c.getStreetBlock().getAngle());			
				translatedXpoints[1]=rotateX(x, y, xpoints[1], ypoints[1], c.getStreetBlock().getAngle());
				translatedXpoints[2]=rotateX(x, y, xpoints[2], ypoints[2], c.getStreetBlock().getAngle());
				translatedYpoints[0]=rotateY(x, y, xpoints[0], ypoints[0], c.getStreetBlock().getAngle());			
				translatedYpoints[1]=rotateY(x, y, xpoints[1], ypoints[1], c.getStreetBlock().getAngle());
				translatedYpoints[2]=rotateY(x, y, xpoints[2], ypoints[2], c.getStreetBlock().getAngle());
				g.fillPolygon(translatedXpoints, translatedYpoints, 3);
			}
		}
		g.dispose();
	}

	public void setCarSize(int size){
		this.carSize=size;
	}
}
