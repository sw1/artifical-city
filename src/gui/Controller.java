package gui;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.swing.JList;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import algorithm.CarGenerator;
import algorithm.CarGenerator.NotEmptyBlock;
import algorithm.ModelTime;
import algorithm.OSMNode;
import algorithm.OsmParser;
import city.CarComposite;
import city.StreetBlock;

/**
 * Main class of program. Controls whole simulation and contains main loop.
 * 
 * @author Adam Świeżowski
 */

public class Controller {
	private Random rand = new Random();
	private MainWindow mainWindow;
	private ControlWindow crtlWindow;
	private boolean enabled=true;
	private CarGenerator carGen;
	private OsmParser parser;
	private CarComposite cars;
	private ModelTime time;
	private int carLimit=0;
	
	public static void main(String args[]) {
		new Controller();
	}
	
	/**
	 * Sets up simulation
	 */
	public Controller(){
		parser=new OsmParser();
		cars=new CarComposite();
		time=ModelTime.getInstance();
		mainWindow=new MainWindow(cars);
		mainWindow.setVisible(true);
		crtlWindow=new ControlWindow(cars);
		crtlWindow.setVisible(true);
		crtlWindow.getStatusTextField().setText("Parsowanie pliku");
		crtlWindow.getStatusTextField().setBackground(Color.ORANGE);
		parser.execute();
		carGen =new CarGenerator(parser.getNodes());
		
		crtlWindow.getStatusTextField().setBackground(Color.GREEN);
		crtlWindow.getStatusTextField().setText("Uruchomiona");
		
		crtlWindow.getStartStopBtn().addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent arg0) {
				if(enabled){
					enabled=false;
					crtlWindow.getStatusTextField().setText("Wstrzymana");
					crtlWindow.getStatusTextField().setBackground(Color.RED);
					time.stop();
				}else{
					enabled=true;
					crtlWindow.getStatusTextField().setBackground(Color.GREEN);
					crtlWindow.getStatusTextField().setText("Uruchomiona");
					time.start();
				}
				
			}
		});
		
		crtlWindow.getSpeedSlider().addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				int value=((JSlider) arg0.getSource()).getValue();
				time.setSpeed((int)(27.*(100./Math.pow(value,1.5))));	
				crtlWindow.getTimeFactorLabel().setText(String.format("%.2fx",time.getTimeFactor()));
			}
		});
		crtlWindow.getCarLimitSlider().setValue(carLimit);
		crtlWindow.getCarLimitSlider().addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				JSlider s=(JSlider) e.getSource();
				carLimit=s.getValue();
			}
		});
		
		crtlWindow.getBlocksList().addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				JList<StreetBlock> l = (JList<StreetBlock>) e.getSource();
				StreetBlock s = (StreetBlock) l.getSelectedValue();
				if (s != null) {
					mainWindow.setStreetBlock(s);
					mainWindow.repaint();
				}
			}
		});
		
		crtlWindow.getNodeCountTextField().setText(OSMNode.getCount().toString());
		crtlWindow.getBlockCountTextField().setText(StreetBlock.getCount().toString());
		crtlWindow.setNodes(parser.getNodes());
		time.start();
		
		run();
		
	}
	
	private void genCar() {
		int newCarCount=rand.nextInt(Math.max(carLimit-cars.getSet().size(),0));
		for (int i=0;i<newCarCount;i++){
			try {
				cars.add(carGen.getRandCar());
			} catch (NotEmptyBlock e) {
			}
		}
	}
	
	public void run() {
		long diffTime=0,time1=0;
		while (true) {
			time1=System.currentTimeMillis();
			if(enabled){
				if(cars.getSet().size()<carLimit){
					genCar();
				}
				cars.setAction();
				cars.move();
				mainWindow.repaint();
				crtlWindow.updateStats();
			}
			diffTime=System.currentTimeMillis()-time1;
			try {
				TimeUnit.MILLISECONDS.sleep(time.getSpeed()-diffTime);
				time1=System.currentTimeMillis();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
