package gui;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;
import javax.swing.JTextField;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import algorithm.ModelTime;
import city.Car;
import city.Statistic;
import city.StreetBlock;

/**
 * Computes and display chart of speeds of cars
 * @author Adam Świeżowski
 *
 */

public class StatPanel extends JPanel implements Observer {

	private static final long serialVersionUID = 6313272980322446283L;
	private XYSeries series;
	private XYSeriesCollection dataset;
	private JFreeChart lineChart;
	private ModelTime timer;
	private StreetBlock block;
	private double speeds=0;
	private int count=0;
	private JTextField speedField;
	private JTextField carCountField;
	int countCars;
	Statistic statTask;

	public StatPanel() {
		timer = ModelTime.getInstance();
		series = new XYSeries("Przepustowosc");
		dataset = new XYSeriesCollection(series);
		lineChart = createChart(dataset);
		ChartPanel chartPanel=new ChartPanel(lineChart);
		GridBagLayout gbc_stat=new GridBagLayout();
		gbc_stat.columnWidths = new int[] { 400 };
		gbc_stat.rowHeights = new int[] {400};
		gbc_stat.columnWeights = new double[] { 1.0 };
		gbc_stat.rowWeights = new double[] {1.0 };
		setLayout(gbc_stat);
		GridBagConstraints gbc=new GridBagConstraints();
		gbc.gridx=0;
		gbc.gridy=0;
		gbc.fill=GridBagConstraints.BOTH;
		add(chartPanel,gbc);
		
	}

	public void setBlock(StreetBlock block) {
		if(this.block!=null)
			this.block.deleteObservers();
		this.block = block;
		dataset.removeAllSeries();
		series = new XYSeries("Przepustowosc");
		dataset.addSeries(series);
		speeds=0.;
		count=0;
		block.addObserver(this);
		speedField.setText("0.0 km/h");
		carCountField.setText("0");
	}

	public void checkBlock() {
		/*if (block != null) {
			if (block.getCar() != null) {
				series.add(timer.getTotalTime(),block.getCar().getSpeed());
				speeds+=block.getCar().getSpeed();
				count++;
				speedField.setText(String.format("%.2f km/h", (speeds/count)*10));
				carCountField.setText(String.valueOf(count));
			}
		}*/
	}
	
	public void addStatistic(Car c){
		series.add(timer.getTotalTime(),c.getSpeed());
		speeds+=c.getSpeed();
		count++;
		speedField.setText(String.format("%.2f km/h", (speeds/count)*10));
		carCountField.setText(String.valueOf(count));
	}

	private JFreeChart createChart(final XYDataset dataset) {
		final JFreeChart lineChart = ChartFactory.createXYLineChart(
				"Przepustowość", "Czas symulacji [s]", "Szybkość [10 km/h]", dataset,
				PlotOrientation.VERTICAL, false, false, false);

		final XYPlot plot = lineChart.getXYPlot();
		plot.setDomainGridlinesVisible(true);
		plot.setDomainGridlinePaint(Color.LIGHT_GRAY);
		plot.setRangeGridlinesVisible(true);
		plot.setRangeGridlinePaint(Color.LIGHT_GRAY);

		ValueAxis xaxis = plot.getDomainAxis();
		xaxis.setRange(0.0, 3600.);
		xaxis.setAutoRange(true);
		//xaxis.setFixedAutoRange(3600.0);
		ValueAxis yaxis = plot.getRangeAxis();
		yaxis.setRange(0.0, 8.0);

		return lineChart;

	}
	
	public void setSpeedField(JTextField speedField){
		this.speedField=speedField;
	}
	
	public void setCarCountField(JTextField carCountField){
		this.carCountField=carCountField;
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		if(arg1 instanceof Car){
			addStatistic((Car) arg1);
		}
	}

}
