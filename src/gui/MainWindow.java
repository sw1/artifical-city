package gui;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JFrame;

import org.jdesktop.swingx.JXMapKit;
import org.jdesktop.swingx.OSMTileFactoryInfo;
import org.jdesktop.swingx.mapviewer.DefaultTileFactory;
import org.jdesktop.swingx.mapviewer.GeoPosition;
import org.jdesktop.swingx.mapviewer.TileFactoryInfo;

import city.CarComposite;
import city.StreetBlock;

public class MainWindow extends JFrame {
	private static final long serialVersionUID = -2182102535189747385L;
	private JXMapKit mapViewer;
	private CarPainter carPainter;

	public MainWindow(CarComposite cars) {
		mapViewer = new JXMapKit();
		// Create a TileFactoryInfo for OpenStreetMap
		TileFactoryInfo info = new OSMTileFactoryInfo();
		DefaultTileFactory tileFactory = new DefaultTileFactory(info);
		mapViewer.setTileFactory(tileFactory);
		// Use 8 threads in parallel to load the tiles
		tileFactory.setThreadPoolSize(8);

		// Set the focus

		GeoPosition mistrzejowice = new GeoPosition(50.0957, 20.0007);

		mapViewer.setZoom(5);

		mapViewer.setAddressLocation(mistrzejowice);

		carPainter = new CarPainter();
		carPainter.setCars(cars.getSet());

		mapViewer.getMainMap().addPropertyChangeListener("zoom",
				new PropertyChangeListener() {
					public void propertyChange(PropertyChangeEvent arg0) {
						// TODO Auto-generated method stub
						int carsize;
						switch (mapViewer.getMainMap().getZoom()) {
						case 5:
							carsize = 2;
							break;
						case 4:
							carsize = 3;
							break;
						case 3:
							carsize = 4;
							break;
						case 2:
							carsize = 6;
							break;
						case 1:
							carsize = 8;
							break;
						default:
							carsize = 2;
						}
						carPainter.setCarSize(carsize);
					}
				});
		mapViewer.getMainMap().setOverlayPainter(carPainter);

		this.getContentPane().add(mapViewer);
		this.setSize(800, 600);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("Symulacja ruchu miejskiego");
	}

	public void repaint() {
		mapViewer.repaint();
	}
	
	public void setStreetBlock(StreetBlock block){
		carPainter.setBlock(block);
	}

}
