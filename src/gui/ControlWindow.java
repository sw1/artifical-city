package gui;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListDataListener;

import algorithm.ModelTime;
import algorithm.OSMNode;
import city.Car;
import city.CarComposite;
import city.StreetBlock;

public class ControlWindow extends JFrame {

	private JPanel contentPane;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField;
	private JTextField textField_3;
	private JTextField textField_6;
	private JTextField txtParsowanie;
	private CarComposite cars;
	private JButton btnNewButton;
	private JButton btnReset;
	private JSlider slider;
	private JComboBox<Car> comboBox;
	private Car watchedCar;
	private JSpinner spinner;
	private JComboBox<Color> comboBox_1;
	private DefaultListModel<Long> model1;
	private JTextField textField_8;
	private JLabel lblx;
	private JList<OSMNode> list;
	private JLabel label_6;
	private JLabel label_5;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_9;
	private JTextField textField_10;
	private JTextField textField_11;
	private JSlider slider_2;
	private JList<StreetBlock> list_1;
	private ModelTime time;
	private Vector<OSMNode> nodes;
	private StatPanel statPanel;

	private class BlocksModel implements ListModel<StreetBlock> {

		Vector<StreetBlock> blocks = new Vector<StreetBlock>(0);

		public void setStretBlocks(Vector<StreetBlock> blocks) {
			this.blocks = blocks;
		}

		public void addListDataListener(ListDataListener l) {
		}

		public StreetBlock getElementAt(int index) {
			return blocks.get(index);
		}

		public int getSize() {
			return blocks.size();
		}

		public void removeListDataListener(ListDataListener l) {
		}

	}

	private BlocksModel streetBlocksModel;
	private JTextField textField_7;
	private JTextField textField_12;

	public void setNodes(Collection<OSMNode> mapNodes) {
		this.nodes = new Vector<OSMNode>(mapNodes);
		Collections.sort(nodes, new Comparator<OSMNode>() {
			public int compare(OSMNode arg0, OSMNode arg1) {
				return arg1.getStreetBlocks().size()
						- arg0.getStreetBlocks().size();
			}
		});
		list.setModel(new ListModel<OSMNode>() {
			public void addListDataListener(ListDataListener l) {
			}

			public OSMNode getElementAt(int index) {
				return nodes.get(index);
			}

			public int getSize() {
				return nodes.size();
			}

			public void removeListDataListener(ListDataListener l) {
			}
		});
	}

	public ControlWindow(CarComposite carsComp) {
		this.cars = carsComp;

		time = ModelTime.getInstance();
		setTitle("Kontrola modelu");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 117, 0, 0, 0, 0 };
		gbl_contentPane.rowHeights = new int[] { 0, 0, 0 };
		gbl_contentPane.columnWeights = new double[] { 0.0, 0.0, 1.0, 0.0, 0.0 };
		gbl_contentPane.rowWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);

		btnNewButton = new JButton("Start/Stop");
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.anchor = GridBagConstraints.WEST;
		gbc_btnNewButton.insets = new Insets(0, 0, 5, 5);
		gbc_btnNewButton.gridx = 0;
		gbc_btnNewButton.gridy = 0;
		contentPane.add(btnNewButton, gbc_btnNewButton);

		JLabel lblStatusSymulacji = new JLabel("Status symulacji");
		GridBagConstraints gbc_lblStatusSymulacji = new GridBagConstraints();
		gbc_lblStatusSymulacji.anchor = GridBagConstraints.EAST;
		gbc_lblStatusSymulacji.insets = new Insets(0, 0, 5, 5);
		gbc_lblStatusSymulacji.gridx = 1;
		gbc_lblStatusSymulacji.gridy = 0;
		contentPane.add(lblStatusSymulacji, gbc_lblStatusSymulacji);

		txtParsowanie = new JTextField();
		txtParsowanie.setText("Parsowanie");
		txtParsowanie.setHorizontalAlignment(SwingConstants.CENTER);
		txtParsowanie.setEditable(false);
		GridBagConstraints gbc_txtParsowanie = new GridBagConstraints();
		gbc_txtParsowanie.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtParsowanie.anchor = GridBagConstraints.WEST;
		gbc_txtParsowanie.insets = new Insets(0, 0, 5, 5);
		gbc_txtParsowanie.gridx = 2;
		gbc_txtParsowanie.gridy = 0;
		contentPane.add(txtParsowanie, gbc_txtParsowanie);
		txtParsowanie.setColumns(10);

		btnReset = new JButton("Reset");
		btnReset.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				cars.removeAll();
				time.resetTime();
			}
		});
		GridBagConstraints gbc_btnReset = new GridBagConstraints();
		gbc_btnReset.insets = new Insets(0, 0, 5, 0);
		gbc_btnReset.gridx = 4;
		gbc_btnReset.gridy = 0;
		contentPane.add(btnReset, gbc_btnReset);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		GridBagConstraints gbc_tabbedPane = new GridBagConstraints();
		gbc_tabbedPane.insets = new Insets(0, 0, 0, 5);
		gbc_tabbedPane.gridwidth = 5;
		gbc_tabbedPane.fill = GridBagConstraints.BOTH;
		gbc_tabbedPane.gridx = 0;
		gbc_tabbedPane.gridy = 1;
		contentPane.add(tabbedPane, gbc_tabbedPane);

		JPanel panel = new JPanel();
		tabbedPane.addTab("Ustawienia", null, panel, null);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[] { 0, 0, 0, 0, 0 };
		gbl_panel.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_panel.columnWeights = new double[] { 0.0, 0.0, 1.0, 0.0,
				Double.MIN_VALUE };
		gbl_panel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		panel.setLayout(gbl_panel);

		lblx = new JLabel("1.00x");
		GridBagConstraints gbc_lblx = new GridBagConstraints();
		gbc_lblx.insets = new Insets(0, 0, 5, 5);
		gbc_lblx.gridx = 2;
		gbc_lblx.gridy = 0;
		panel.add(lblx, gbc_lblx);

		JLabel lblNewLabel_6 = new JLabel("Szybkość symulacji");
		GridBagConstraints gbc_lblNewLabel_6 = new GridBagConstraints();
		gbc_lblNewLabel_6.anchor = GridBagConstraints.NORTH;
		gbc_lblNewLabel_6.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_6.gridx = 0;
		gbc_lblNewLabel_6.gridy = 1;
		panel.add(lblNewLabel_6, gbc_lblNewLabel_6);

		JLabel label = new JLabel("0");
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.anchor = GridBagConstraints.EAST;
		gbc_label.insets = new Insets(0, 0, 5, 5);
		gbc_label.gridx = 1;
		gbc_label.gridy = 1;
		panel.add(label, gbc_label);

		slider = new JSlider();
		slider.setMinimum(1);
		slider.setValue(1);
		GridBagConstraints gbc_slider = new GridBagConstraints();
		gbc_slider.fill = GridBagConstraints.HORIZONTAL;
		gbc_slider.anchor = GridBagConstraints.NORTH;
		gbc_slider.insets = new Insets(0, 0, 5, 5);
		gbc_slider.gridx = 2;
		gbc_slider.gridy = 1;
		panel.add(slider, gbc_slider);

		JLabel label_1 = new JLabel("100");
		GridBagConstraints gbc_label_1 = new GridBagConstraints();
		gbc_label_1.anchor = GridBagConstraints.WEST;
		gbc_label_1.insets = new Insets(0, 0, 5, 0);
		gbc_label_1.gridx = 3;
		gbc_label_1.gridy = 1;
		panel.add(label_1, gbc_label_1);
		
				JLabel lblNewLabel_12 = new JLabel("Czas");
				GridBagConstraints gbc_lblNewLabel_12 = new GridBagConstraints();
				gbc_lblNewLabel_12.insets = new Insets(0, 0, 5, 5);
				gbc_lblNewLabel_12.gridx = 0;
				gbc_lblNewLabel_12.gridy = 2;
				panel.add(lblNewLabel_12, gbc_lblNewLabel_12);

		time.addChangeEvent(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				ModelTime t = (ModelTime) e.getSource();
				label_6.setText(t.getHours() + ":"
						+ String.format("%02d", t.getMinutes()));

			}
		});
		
				label_6 = new JLabel("00:00");
				GridBagConstraints gbc_label_6 = new GridBagConstraints();
				gbc_label_6.insets = new Insets(0, 0, 5, 5);
				gbc_label_6.gridx = 2;
				gbc_label_6.gridy = 2;
				panel.add(label_6, gbc_label_6);

		JLabel lblNewLabel_7 = new JLabel("Ilość samochodów");
		GridBagConstraints gbc_lblNewLabel_7 = new GridBagConstraints();
		gbc_lblNewLabel_7.anchor = GridBagConstraints.NORTHEAST;
		gbc_lblNewLabel_7.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_7.gridx = 0;
		gbc_lblNewLabel_7.gridy = 5;
		panel.add(lblNewLabel_7, gbc_lblNewLabel_7);

		textField_6 = new JTextField();
		textField_6.setEditable(false);
		GridBagConstraints gbc_textField_6 = new GridBagConstraints();
		gbc_textField_6.insets = new Insets(0, 0, 5, 5);
		gbc_textField_6.gridx = 2;
		gbc_textField_6.gridy = 5;
		panel.add(textField_6, gbc_textField_6);
		textField_6.setColumns(10);

		cars.setChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				CarComposite cc = (CarComposite) arg0.getSource();
				textField_6.setText(String.valueOf(cc.getSet().size()));
			}
		});

		JLabel lblNewLabel_8 = new JLabel("Ilość węzłów");
		GridBagConstraints gbc_lblNewLabel_8 = new GridBagConstraints();
		gbc_lblNewLabel_8.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_8.gridx = 0;
		gbc_lblNewLabel_8.gridy = 6;
		panel.add(lblNewLabel_8, gbc_lblNewLabel_8);

		textField = new JTextField();
		textField.setEditable(false);
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_textField.gridx = 2;
		gbc_textField.gridy = 6;
		panel.add(textField, gbc_textField);
		textField.setColumns(10);

		JLabel lblNewLabel_9 = new JLabel("Ilość bloczków");
		GridBagConstraints gbc_lblNewLabel_9 = new GridBagConstraints();
		gbc_lblNewLabel_9.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_9.gridx = 0;
		gbc_lblNewLabel_9.gridy = 7;
		panel.add(lblNewLabel_9, gbc_lblNewLabel_9);

		textField_3 = new JTextField();
		textField_3.setEditable(false);
		GridBagConstraints gbc_textField_3 = new GridBagConstraints();
		gbc_textField_3.insets = new Insets(0, 0, 5, 5);
		gbc_textField_3.gridx = 2;
		gbc_textField_3.gridy = 7;
		panel.add(textField_3, gbc_textField_3);
		textField_3.setColumns(10);

		label_5 = new JLabel("3000");
		GridBagConstraints gbc_label_5 = new GridBagConstraints();
		gbc_label_5.insets = new Insets(0, 0, 5, 5);
		gbc_label_5.gridx = 2;
		gbc_label_5.gridy = 8;
		panel.add(label_5, gbc_label_5);

		JLabel lblNewLabel_10 = new JLabel("Ilość samochodów");
		GridBagConstraints gbc_lblNewLabel_10 = new GridBagConstraints();
		gbc_lblNewLabel_10.insets = new Insets(0, 0, 0, 5);
		gbc_lblNewLabel_10.gridx = 0;
		gbc_lblNewLabel_10.gridy = 9;
		panel.add(lblNewLabel_10, gbc_lblNewLabel_10);

		JLabel label_3 = new JLabel("0");
		GridBagConstraints gbc_label_3 = new GridBagConstraints();
		gbc_label_3.insets = new Insets(0, 0, 0, 5);
		gbc_label_3.gridx = 1;
		gbc_label_3.gridy = 9;
		panel.add(label_3, gbc_label_3);

		slider_2 = new JSlider();
		slider_2.setMaximum(6000);
		slider_2.setValue(3000);
		slider_2.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				JSlider s = (JSlider) arg0.getSource();
				label_5.setText(String.valueOf(s.getValue()));
			}
		});
		GridBagConstraints gbc_slider_2 = new GridBagConstraints();
		gbc_slider_2.fill = GridBagConstraints.HORIZONTAL;
		gbc_slider_2.insets = new Insets(0, 0, 0, 5);
		gbc_slider_2.gridx = 2;
		gbc_slider_2.gridy = 9;
		panel.add(slider_2, gbc_slider_2);

		JLabel label_4 = new JLabel("6000");
		GridBagConstraints gbc_label_4 = new GridBagConstraints();
		gbc_label_4.anchor = GridBagConstraints.WEST;
		gbc_label_4.gridx = 3;
		gbc_label_4.gridy = 9;
		panel.add(label_4, gbc_label_4);

		JPanel panel_2 = new JPanel();
		panel_2.addComponentListener(new ComponentAdapter() {
			public void componentShown(ComponentEvent e) {
				setComboBox();
			}

			public void componentHidden(ComponentEvent e) {
				if (watchedCar != null)
					watchedCar.removeChangeListener();
				if (comboBox != null)
					comboBox.removeAllItems();
			}
		});

		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Węzły", null, panel_1, null);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[] { 229, 14, 80, 89, 40, 87 };
		gbl_panel_1.rowHeights = new int[] { 0, 0, 26, 0, 0, 31, 0, 0, 0 };
		gbl_panel_1.columnWeights = new double[] { 1.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
		gbl_panel_1.rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 0.0, 0.0,
				0.0, 1.0, Double.MIN_VALUE };
		panel_1.setLayout(gbl_panel_1);

		JLabel lblNewLabel = new JLabel("Węzeł");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 0;
		panel_1.add(lblNewLabel, gbc_lblNewLabel);

		JSeparator separator = new JSeparator();
		separator.setOrientation(SwingConstants.VERTICAL);
		GridBagConstraints gbc_separator = new GridBagConstraints();
		gbc_separator.fill = GridBagConstraints.VERTICAL;
		gbc_separator.gridheight = 8;
		gbc_separator.insets = new Insets(0, 0, 5, 5);
		gbc_separator.gridx = 1;
		gbc_separator.gridy = 0;
		panel_1.add(separator, gbc_separator);

		JLabel lblWsprzdne = new JLabel("Współrzędne");
		GridBagConstraints gbc_lblWsprzdne = new GridBagConstraints();
		gbc_lblWsprzdne.gridwidth = 4;
		gbc_lblWsprzdne.insets = new Insets(0, 0, 5, 0);
		gbc_lblWsprzdne.gridx = 2;
		gbc_lblWsprzdne.gridy = 0;
		panel_1.add(lblWsprzdne, gbc_lblWsprzdne);

		list = new JList<OSMNode>();
		list.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				JList<OSMNode> l = (JList<OSMNode>) e.getSource();
				OSMNode n = (OSMNode) l.getSelectedValue();
				if(n!=null){
				textField_4.setText(Double.toString(n.getLatitude()));
				textField_5.setText(Double.toString(n.getLongitude()));
				textField_10.setText(Integer.toString(n.getProbability()));
				streetBlocksModel.setStretBlocks(n.getStreetBlocks());
				list_1.updateUI();
				list_1.clearSelection();
				textField_9.setText("");
				}
			}
		});
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		JScrollPane scrollPane = new JScrollPane(list);
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.weightx = 1.2;
		gbc_scrollPane.gridheight = 7;
		gbc_scrollPane.insets = new Insets(0, 0, 5, 5);
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 1;
		panel_1.add(scrollPane, gbc_scrollPane);

		JLabel lblSzeroko = new JLabel("Szerokość:");
		GridBagConstraints gbc_lblSzeroko = new GridBagConstraints();
		gbc_lblSzeroko.anchor = GridBagConstraints.EAST;
		gbc_lblSzeroko.insets = new Insets(0, 0, 5, 5);
		gbc_lblSzeroko.gridx = 2;
		gbc_lblSzeroko.gridy = 1;
		panel_1.add(lblSzeroko, gbc_lblSzeroko);

		textField_4 = new JTextField();
		textField_4.setText("0.000");
		textField_4.setEditable(false);
		GridBagConstraints gbc_textField_4 = new GridBagConstraints();
		gbc_textField_4.insets = new Insets(0, 0, 5, 5);
		gbc_textField_4.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_4.gridx = 3;
		gbc_textField_4.gridy = 1;
		panel_1.add(textField_4, gbc_textField_4);
		textField_4.setColumns(10);

		JLabel lblDugo = new JLabel("Długość:");
		GridBagConstraints gbc_lblDugo = new GridBagConstraints();
		gbc_lblDugo.anchor = GridBagConstraints.EAST;
		gbc_lblDugo.insets = new Insets(0, 0, 5, 5);
		gbc_lblDugo.gridx = 4;
		gbc_lblDugo.gridy = 1;
		panel_1.add(lblDugo, gbc_lblDugo);

		textField_5 = new JTextField();
		textField_5.setEditable(false);
		textField_5.setText("0.000");
		GridBagConstraints gbc_textField_5 = new GridBagConstraints();
		gbc_textField_5.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_5.insets = new Insets(0, 0, 5, 0);
		gbc_textField_5.gridx = 5;
		gbc_textField_5.gridy = 1;
		panel_1.add(textField_5, gbc_textField_5);
		textField_5.setColumns(10);

		JLabel lblNewLabel_1 = new JLabel("Bloczki");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.gridwidth = 4;
		gbc_lblNewLabel_1.fill = GridBagConstraints.VERTICAL;
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel_1.gridx = 2;
		gbc_lblNewLabel_1.gridy = 2;
		panel_1.add(lblNewLabel_1, gbc_lblNewLabel_1);

		list_1 = new JList<StreetBlock>();
		list_1.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				JList<StreetBlock> l = (JList<StreetBlock>) e.getSource();
				StreetBlock s = (StreetBlock) l.getSelectedValue();
				if (s != null) {
					textField_11.setText(Integer.toString(s.getMaxSpeed()));
					if (s.getNextNode() != null) {
						textField_9.setText(s.getNextNode().toString());
					} else {
						textField_9.setText("");
					}
					statPanel.setBlock(s);
				}
			}
		});
		streetBlocksModel = new BlocksModel();
		list_1.setModel(streetBlocksModel);
		GridBagConstraints gbc_list_1 = new GridBagConstraints();
		gbc_list_1.weightx = 1.0;
		gbc_list_1.insets = new Insets(0, 0, 5, 0);
		gbc_list_1.gridwidth = 4;
		gbc_list_1.fill = GridBagConstraints.BOTH;
		gbc_list_1.gridx = 2;
		gbc_list_1.gridy = 3;
		panel_1.add(list_1, gbc_list_1);

		JLabel lblKierunek = new JLabel("Kierunek");
		GridBagConstraints gbc_lblKierunek = new GridBagConstraints();
		gbc_lblKierunek.anchor = GridBagConstraints.EAST;
		gbc_lblKierunek.insets = new Insets(0, 0, 5, 5);
		gbc_lblKierunek.gridx = 2;
		gbc_lblKierunek.gridy = 4;
		panel_1.add(lblKierunek, gbc_lblKierunek);

		textField_9 = new JTextField();
		textField_9.setEditable(false);
		GridBagConstraints gbc_textField_9 = new GridBagConstraints();
		gbc_textField_9.gridwidth = 2;
		gbc_textField_9.insets = new Insets(0, 0, 5, 5);
		gbc_textField_9.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_9.gridx = 3;
		gbc_textField_9.gridy = 4;
		panel_1.add(textField_9, gbc_textField_9);
		textField_9.setColumns(10);

		JButton btnIdDo = new JButton("Idź do");
		GridBagConstraints gbc_btnIdDo = new GridBagConstraints();
		gbc_btnIdDo.insets = new Insets(0, 0, 5, 0);
		gbc_btnIdDo.gridx = 5;
		gbc_btnIdDo.gridy = 4;
		panel_1.add(btnIdDo, gbc_btnIdDo);

		JLabel lblPrawdopodobiestwo = new JLabel("Prawdopodobieństwo");
		GridBagConstraints gbc_lblPrawdopodobiestwo = new GridBagConstraints();
		gbc_lblPrawdopodobiestwo.gridwidth = 2;
		gbc_lblPrawdopodobiestwo.anchor = GridBagConstraints.EAST;
		gbc_lblPrawdopodobiestwo.insets = new Insets(0, 0, 5, 5);
		gbc_lblPrawdopodobiestwo.gridx = 2;
		gbc_lblPrawdopodobiestwo.gridy = 5;
		panel_1.add(lblPrawdopodobiestwo, gbc_lblPrawdopodobiestwo);

		textField_10 = new JTextField();
		textField_10.setEditable(false);
		GridBagConstraints gbc_textField_10 = new GridBagConstraints();
		gbc_textField_10.insets = new Insets(0, 0, 5, 5);
		gbc_textField_10.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_10.gridx = 4;
		gbc_textField_10.gridy = 5;
		panel_1.add(textField_10, gbc_textField_10);
		textField_10.setColumns(10);

		JLabel lblSzybko = new JLabel("Szybkość maksymalna");
		GridBagConstraints gbc_lblSzybko = new GridBagConstraints();
		gbc_lblSzybko.gridwidth = 2;
		gbc_lblSzybko.anchor = GridBagConstraints.EAST;
		gbc_lblSzybko.insets = new Insets(0, 0, 5, 5);
		gbc_lblSzybko.gridx = 2;
		gbc_lblSzybko.gridy = 6;
		panel_1.add(lblSzybko, gbc_lblSzybko);

		textField_11 = new JTextField();
		textField_11.setEditable(false);
		GridBagConstraints gbc_textField_11 = new GridBagConstraints();
		gbc_textField_11.insets = new Insets(0, 0, 5, 5);
		gbc_textField_11.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_11.gridx = 4;
		gbc_textField_11.gridy = 6;
		panel_1.add(textField_11, gbc_textField_11);
		textField_11.setColumns(10);

		tabbedPane.addTab("Samochody", null, panel_2, null);

		GridBagLayout gbl_panel_2 = new GridBagLayout();
		gbl_panel_2.columnWidths = new int[] { 0, 0, 0 };
		gbl_panel_2.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 131, 0 };
		gbl_panel_2.columnWeights = new double[] { 1.0, 1.0, Double.MIN_VALUE };
		gbl_panel_2.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0, 0.0, Double.MIN_VALUE };
		panel_2.setLayout(gbl_panel_2);

		JLabel lblNewLabel_2 = new JLabel("Samochód");
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2.gridx = 0;
		gbc_lblNewLabel_2.gridy = 0;
		panel_2.add(lblNewLabel_2, gbc_lblNewLabel_2);

		comboBox = new JComboBox<Car>();
		GridBagConstraints gbc_comboBox = new GridBagConstraints();
		gbc_comboBox.insets = new Insets(0, 0, 5, 0);
		gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox.gridx = 1;
		gbc_comboBox.gridy = 0;
		panel_2.add(comboBox, gbc_comboBox);
		comboBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (watchedCar != null) {
					watchedCar.removeChangeListener();
				}
				watchedCar = (Car) arg0.getItem();
				watchedCar.setChangeListener(new ChangeListener() {
					public void stateChanged(ChangeEvent arg0) {
						setCarInfo((Car) arg0.getSource());
					}
				});

			}
		});

		JLabel lblNewLabel_3 = new JLabel("Szybkość");
		GridBagConstraints gbc_lblNewLabel_3 = new GridBagConstraints();
		gbc_lblNewLabel_3.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_3.gridx = 0;
		gbc_lblNewLabel_3.gridy = 1;
		panel_2.add(lblNewLabel_3, gbc_lblNewLabel_3);

		spinner = new JSpinner();
		GridBagConstraints gbc_spinner = new GridBagConstraints();
		gbc_spinner.anchor = GridBagConstraints.EAST;
		gbc_spinner.insets = new Insets(0, 0, 5, 0);
		gbc_spinner.gridx = 1;
		gbc_spinner.gridy = 1;
		panel_2.add(spinner, gbc_spinner);

		JLabel lblNewLabel_4 = new JLabel("Położenie");
		GridBagConstraints gbc_lblNewLabel_4 = new GridBagConstraints();
		gbc_lblNewLabel_4.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_4.gridx = 0;
		gbc_lblNewLabel_4.gridy = 2;
		panel_2.add(lblNewLabel_4, gbc_lblNewLabel_4);

		textField_1 = new JTextField();
		textField_1.setEditable(false);
		GridBagConstraints gbc_textField_1 = new GridBagConstraints();
		gbc_textField_1.insets = new Insets(0, 0, 5, 0);
		gbc_textField_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_1.gridx = 1;
		gbc_textField_1.gridy = 2;
		panel_2.add(textField_1, gbc_textField_1);
		textField_1.setColumns(10);

		JLabel lblNewLabel_5 = new JLabel("Kierunek");
		GridBagConstraints gbc_lblNewLabel_5 = new GridBagConstraints();
		gbc_lblNewLabel_5.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_5.gridx = 0;
		gbc_lblNewLabel_5.gridy = 3;
		panel_2.add(lblNewLabel_5, gbc_lblNewLabel_5);

		textField_2 = new JTextField();
		textField_2.setEditable(false);
		GridBagConstraints gbc_textField_2 = new GridBagConstraints();
		gbc_textField_2.insets = new Insets(0, 0, 5, 0);
		gbc_textField_2.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_2.gridx = 1;
		gbc_textField_2.gridy = 3;
		panel_2.add(textField_2, gbc_textField_2);
		textField_2.setColumns(10);

		JLabel lblKolor = new JLabel("Kolor");
		GridBagConstraints gbc_lblKolor = new GridBagConstraints();
		gbc_lblKolor.insets = new Insets(0, 0, 5, 5);
		gbc_lblKolor.gridx = 0;
		gbc_lblKolor.gridy = 4;
		panel_2.add(lblKolor, gbc_lblKolor);

		comboBox_1 = new JComboBox<Color>();
		comboBox_1.addItem(Color.GREEN);
		comboBox_1.addItem(Color.BLACK);
		comboBox_1.addItem(Color.RED);
		comboBox_1.addItem(Color.BLUE);
		comboBox_1.addItem(Color.WHITE);
		comboBox_1.addItem(Color.YELLOW);
		GridBagConstraints gbc_comboBox_1 = new GridBagConstraints();
		gbc_comboBox_1.insets = new Insets(0, 0, 5, 0);
		gbc_comboBox_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox_1.gridx = 1;
		gbc_comboBox_1.gridy = 4;
		panel_2.add(comboBox_1, gbc_comboBox_1);
		comboBox_1.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (arg0.getStateChange() == ItemEvent.SELECTED) {
					if (watchedCar != null) {
						watchedCar.setColor((Color) arg0.getItem());
					}
				}
			}
		});

		textField_8 = new JTextField();
		textField_8.setEditable(false);
		GridBagConstraints gbc_textField_8 = new GridBagConstraints();
		gbc_textField_8.insets = new Insets(0, 0, 0, 5);
		gbc_textField_8.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_8.gridx = 0;
		gbc_textField_8.gridy = 7;
		panel_2.add(textField_8, gbc_textField_8);
		textField_8.setColumns(10);

		JPanel panel_3 = new JPanel();
		statPanel = new StatPanel();
		
		tabbedPane.addTab("Wykresy", null, panel_3, null);
		GridBagLayout gbl_panel_3 = new GridBagLayout();
		gbl_panel_3.columnWidths = new int[] { 100, 0, 0, 0, 0 };
		gbl_panel_3.rowHeights = new int[] { 0, 0, 400, 0 };
		gbl_panel_3.columnWeights = new double[] { 1.0, 1.0, 0.0, 1.0, Double.MIN_VALUE };
		gbl_panel_3.rowWeights = new double[] { 0.0, 0.0, 1.0, Double.MIN_VALUE };
		panel_3.setLayout(gbl_panel_3);

		JLabel lblWybrBloczka = new JLabel(
				"Proszę wybrać bloczek w zakładce węzły");
		GridBagConstraints gbc_lblWybrBloczka = new GridBagConstraints();
		gbc_lblWybrBloczka.gridwidth = 4;
		gbc_lblWybrBloczka.insets = new Insets(0, 0, 5, 5);
		gbc_lblWybrBloczka.gridx = 0;
		gbc_lblWybrBloczka.gridy = 0;
		panel_3.add(lblWybrBloczka, gbc_lblWybrBloczka);
		
		JLabel lblredniaSzybko = new JLabel("Średnia szybkość:");
		GridBagConstraints gbc_lblredniaSzybko = new GridBagConstraints();
		gbc_lblredniaSzybko.anchor = GridBagConstraints.EAST;
		gbc_lblredniaSzybko.insets = new Insets(0, 0, 5, 5);
		gbc_lblredniaSzybko.gridx = 0;
		gbc_lblredniaSzybko.gridy = 1;
		panel_3.add(lblredniaSzybko, gbc_lblredniaSzybko);
		
		textField_7 = new JTextField();
		textField_7.setEditable(false);
		GridBagConstraints gbc_textField_7 = new GridBagConstraints();
		gbc_textField_7.insets = new Insets(0, 0, 5, 5);
		gbc_textField_7.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_7.gridx = 1;
		gbc_textField_7.gridy = 1;
		panel_3.add(textField_7, gbc_textField_7);
		textField_7.setColumns(10);
		
		JLabel lblIloSamochodw = new JLabel("Ilość samochodów");
		GridBagConstraints gbc_lblIloSamochodw = new GridBagConstraints();
		gbc_lblIloSamochodw.anchor = GridBagConstraints.EAST;
		gbc_lblIloSamochodw.insets = new Insets(0, 0, 5, 5);
		gbc_lblIloSamochodw.gridx = 2;
		gbc_lblIloSamochodw.gridy = 1;
		panel_3.add(lblIloSamochodw, gbc_lblIloSamochodw);
		
		textField_12 = new JTextField();
		textField_12.setEditable(false);
		GridBagConstraints gbc_textField_12 = new GridBagConstraints();
		gbc_textField_12.insets = new Insets(0, 0, 5, 0);
		gbc_textField_12.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_12.gridx = 3;
		gbc_textField_12.gridy = 1;
		panel_3.add(textField_12, gbc_textField_12);
		textField_12.setColumns(10);

		GridBagConstraints gbc_statPanel = new GridBagConstraints();
		gbc_statPanel.insets = new Insets(0, 0, 0, 5);
		gbc_statPanel.gridwidth = 4;
		gbc_statPanel.anchor = GridBagConstraints.NORTHEAST;
		gbc_statPanel.fill = GridBagConstraints.BOTH;
		gbc_statPanel.gridx = 0;
		gbc_statPanel.gridy = 2;
		panel_3.add(statPanel, gbc_statPanel);

		statPanel.setSpeedField(textField_7);
		statPanel.setCarCountField(textField_12);
	}

	private void setCarInfo(Car c) {
		spinner.setValue(c.getSpeed());
		textField_1.setText(c.getStreetBlock().getLatitude().toString() + " "
				+ c.getStreetBlock().getLongitude().toString());
		textField_2.setText(c.getAction().getMoveDirection().toString());
		if (c.getColor() != null)
			comboBox_1.setSelectedItem(c.getColor());
	}

	private void setComboBox() {
		synchronized(cars){
			for (Car c : cars.getSet()) {
				comboBox.addItem(c);
			}
		}
	}

	public JTextField getStatusTextField() {
		return txtParsowanie;
	}

	public JButton getStartStopBtn() {
		return btnNewButton;
	}

	public JSlider getSpeedSlider() {
		return slider;
	}

	public JTextField getNodeCountTextField() {
		return textField;
	}

	public JTextField getBlockCountTextField() {
		return textField_3;
	}

	public JSlider getCarLimitSlider() {
		return slider_2;
	}

	public JLabel getTimeFactorLabel() {
		return lblx;
	}

	public StatPanel getStatPanel() {
		return statPanel;
	}
	public void updateStats(){
		statPanel.checkBlock();
	}
	public JList getBlocksList() {
		return list_1;
	}
}
