package city;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import algorithm.ActionPlaner.EndOfRoadException;

/**
 * Class ease actions on many Cars at once. It implements Composite pattern.
 * 
 * @author Adam Świeżowski
 */

public class CarComposite extends CarComponent {
	private Set<Car> cars;
	private ChangeListener listener;

	public CarComposite() {
		cars = Collections.synchronizedSet(new HashSet<Car>());
	}

	public void add(Car c) {
		cars.add(c);
		if (listener != null) {
			listener.stateChanged(new ChangeEvent(this));
		}
	}

	public void remove(Car c) {
		cars.remove(c);
	}

	public void move() {
		for (Car c : cars) {
			c.move();
		}
		algorithm.unlockNodes();
		algorithm.unlockStreetBlocks();
	}

	public Car getChild(int index) {
		return null;
	}

	public void setAction() {
		Car c = null;
		synchronized (cars) {
			Iterator<Car> cIter = cars.iterator();
			while (cIter.hasNext()) {
				c = cIter.next();
				try {
					algorithm.setNextAction(c);
				} catch (EndOfRoadException e) {
					c.getStreetBlock().setCar(null);
					cIter.remove();
					if (listener != null) {
						listener.stateChanged(new ChangeEvent(this));
					}
				}
			}
		}
	}

	public Set<Car> getSet() {
		return cars;
	}

	public void setChangeListener(ChangeListener listener) {
		this.listener = listener;
	}

	public void removeAll() {
		synchronized (cars) {
			Iterator<Car> cIter = cars.iterator();
			while (cIter.hasNext()) {
				cIter.next().getStreetBlock().setCar(null);
				;
				cIter.remove();
			}
			if (listener != null) {
				listener.stateChanged(new ChangeEvent(this));
			}
		}
	}
}
