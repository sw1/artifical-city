package city;

/**
 * Represents of traffic lights
 * @author Adam Świeżowski
 */
public enum TrafficLights {
	RED,GREEN,NONE
}
