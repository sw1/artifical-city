package city;

/**
 * Class stores next move of Car
 * 
 * @author Adam Świeżowski
 */
public class Action {
	private Long moveDirection;
	private StreetBlock nextBlock;
	private Integer speed;
	
	public Long getMoveDirection() {
		return moveDirection;
	}
	public void setMoveDirection(Long moveDirection) {
		this.moveDirection = moveDirection;
	}
	public StreetBlock getNextBlock() {
		return nextBlock;
	}
	public void setNextBlock(StreetBlock nextBlock) {
		this.nextBlock = nextBlock;
	}
	public Integer getSpeed() {
		return speed;
	}
	public void setSpeed(Integer speed) {
		this.speed = speed;
	}
	

}
