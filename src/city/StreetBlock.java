package city;

import java.util.Observable;

import algorithm.OSMNode;

/**
 * Class represents one StreetBlock. It is basic essential entity of simulation
 * 
 * @author Adam Świeżowski
 */

public class StreetBlock extends Observable{
	private StreetBlock forward, left, right, backward;
	private double latitude, longitude;
	private Integer maxSpeed;
	private Car car = null;
	private TrafficLights trafficLights = TrafficLights.NONE;
	private boolean junction = false;
	private Double angle;
	public OSMNode node;
	private OSMNode backwardNode;
	private OSMNode nextNode;
	private boolean locked=false;
	private static int count=0;
	private String name=null;
	
	/**
	 * @return tilt of block
	 */
	public Double getAngle() {
		return angle;
	}

	public void setAngle(Double angle) {
		this.angle = angle;
	}

	public Double getLatitude() {
		return latitude;
	}

	public StreetBlock() {
		count++;
	}

	public StreetBlock(Double latitude, Double longitude, Integer maxSpeed,
			TrafficLights trafficLights, Boolean junction, Double angle) {
		this(null, null, null, null, latitude, longitude, maxSpeed,
				trafficLights, junction, angle);
	}

	public StreetBlock(StreetBlock forward, StreetBlock left,
			StreetBlock right, StreetBlock backward, Double latitude,
			Double longitude, Integer maxSpeed, TrafficLights trafficLights,
			Boolean junction, Double angle) {
		count++;
		this.forward = forward;
		this.left = left;
		this.right = right;
		this.backward = backward;
		this.latitude = latitude;
		this.longitude = longitude;
		this.maxSpeed = maxSpeed;
		this.trafficLights = trafficLights;
		this.junction = junction;
		this.angle = angle;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public TrafficLights getTrafficLights() {
		return trafficLights;
	}

	public void setTrafficLights(TrafficLights trafficLights) {
		this.trafficLights = trafficLights;
	}

	public boolean isJunction() {
		return junction;
	}

	public void setJunction(boolean junction) {
		this.junction = junction;
	}

	public void setMaxSpeed(Integer maxSpeed) {
		this.maxSpeed = maxSpeed;
	}

	public Integer getMaxSpeed() {
		return maxSpeed;
	}

	/**
	 * Set block occupied by Car
	 * @param car
	 */
	public void setCar(Car car) {
		this.car = car;
	}

	public boolean isEmpty() {
		return car == null;
	}

	public StreetBlock getForward() {
		return forward;
	}

	public void setForward(StreetBlock block) {
		this.forward = block;
	}

	public void setBackward(StreetBlock block) {
		this.backward = block;
	}

	public void setLeft(StreetBlock block) {
		this.left = block;
	}

	public void setRight(StreetBlock block) {
		this.right = block;
	}

	public StreetBlock getBackward() {
		return backward;
	}
	
	/**
	 * @return number of instances of StreetBlock
	 */
	static public Integer getCount(){
		return count;
	}

	/**
	 * @return OSMNode from which block start
	 */
	public OSMNode getBackwardNode() {
		return backwardNode;
	}

	public void setBackwardNode(OSMNode backwardNode) {
		this.backwardNode = backwardNode;
	}

	/**
	 * @return OSMNode to which road lead
	 */
	public OSMNode getNextNode() {
		return nextNode;
	}

	public String getName(){
		return name;
	}
	public void setNextNode(OSMNode nextNode) {
		this.nextNode = nextNode;
	}
	
	/**
	 * Lock so no Car can occupy position
	 */
	public void lock(){
		locked=true;
	}
	
	public void unlock(){
		locked=false;
	}
	
	public boolean isLocked(){
		return locked || car!=null;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Car getCar() {
		return car;
	}

	public String toString(){
		if(name==null){
			StringBuilder s=new StringBuilder();
			s.append(nextNode==null?"":nextNode).append("->").append(backwardNode==null?"":backwardNode);
			return s.toString();
		}
		return name;
	}

	public StreetBlock getLeft() {
		return left;
	}

	public StreetBlock getRight() {
		return right;
	}
	
	public void changed(){
		setChanged();
	}

}
