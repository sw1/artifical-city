package city;

//watek zlicza liczbe samochodow w wybranym streetblock

public class Statistic implements Runnable {
	private StreetBlock streetBlock;
	private Car tmpCar;
	private int counter;
	
	Statistic(){
	}
	
	public Statistic(StreetBlock stblock, int counter) {
		this.counter = counter;
		tmpCar = null;
		streetBlock = stblock;
		
	}

	@Override
	public void run() {
		checkAndAddValue();
		
	}
	
	private void checkAndAddValue(){
		if(streetBlock.isEmpty() && tmpCar != streetBlock.getCar()){
			counter++;
		}
	}
	
	public int getCounter(){
		return counter;
	}
	

}
