package city;

import java.awt.Color;
import java.util.Random;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import algorithm.ActionPlaner.EndOfRoadException;

/**
 * Class represents Car. Car consist speed and position (StreetBlock)
 * 
 * @author Adam Świeżowski
 */

public class Car extends CarComponent {
	private StreetBlock position;
	private Integer speed = 0;
	private Long moveDirection = 0L;
	private Action nextMove;
	private ChangeListener listener;
	private Color color;
	private static Random rand = new Random();

	/**
	 * Creates Car with random color
	 */
	public Car() {
		nextMove = new Action();
		nextMove.setSpeed(speed);
		nextMove.setMoveDirection(0L);
		color = new Color(rand.nextInt(255), rand.nextInt(255),
				rand.nextInt(255));
	}

	/**
	 * Set position and speed of Car from nextMove field
	 */
	public void move() {
		position.setCar(null);
		speed = nextMove.getSpeed();
		moveDirection = nextMove.getMoveDirection();
		position = nextMove.getNextBlock();
		position.setCar(this);
		if (listener != null) {
			listener.stateChanged(new ChangeEvent(this));
		}
	}

	public StreetBlock getStreetBlock() {
		return this.position;
	}

	public Integer getSpeed() {
		return speed;
	}

	public void setSpeed(Integer carSpeed) {
		speed = carSpeed;
	}

	/**
	 * Set nextMove of Car
	 */
	public void setAction() throws EndOfRoadException {
		algorithm.setNextAction(this);

	}

	public Action getAction() {
		return nextMove;
	}

	public Long getMoveDirection() {
		return moveDirection;
	}

	public void setStreetBlock(StreetBlock block) {
		this.position = block;
	}

	public void setChangeListener(ChangeListener listener) {
		this.listener = listener;
	}

	public void removeChangeListener() {
		this.listener = null;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public Color getColor() {
		return color;
	}
}