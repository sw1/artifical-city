package city;

import algorithm.ActionPlaner;
import algorithm.ActionPlaner.EndOfRoadException;

public abstract class CarComponent{
	
	protected static ActionPlaner algorithm=new ActionPlaner();
	
	public void add(Car c){
		
	}
	
	public void remove(Car c){
		
	}
	
	public abstract void setAction() throws EndOfRoadException;
	
	abstract void move();
	
	public Car getChild(int index){
		return null;
	}
}
