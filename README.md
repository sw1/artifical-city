Application is intended to simulate motor traffic in area read from OpenStreetMap xml file. Application gui is currently in polish language. It uses modified NaSch model.

### Feautures ###
* Selecting simulation speed and car count
* Multiple lanes for cars
* Charts of select point of road

### Compilation ###

Simple use maven to compile sources
```
#!sh
maven package
```

You will find compiled jar in target folder

### Dependencies ###
* JxMapViewer2
* JFreeChart

Dependencies are automatically handled with maven

### Running ###

Place highways.osm file in application folder. Then run:

```
#!sh
java -jar Artifical_City-0.0.1-SNAPSHOT-jar-with-dependencies.jar
```

Or download app: [Artifical city](https://bitbucket.org/sw1/artifical-city/src/6c9f996ae1cc0167cadb94c4a5b2a609204966e8/target/Artifical_City-0.0.1-SNAPSHOT-jar-with-dependencies.jar?at=master) and [highway.osm](https://bitbucket.org/sw1/artifical-city/raw/6c9f996ae1cc0167cadb94c4a5b2a609204966e8/highways.osm) 

### Who do I talk to? ###

* Feel free to fork or contact me (the repo owner)

### Screenshots ###

[![app_view.png](https://bitbucket.org/repo/zndRLy/images/4072355812-app_view.png)](https://bitbucket.org/repo/zndRLy/images/4072355812-app_view.png)
[![app_view_nodes.png](https://bitbucket.org/repo/zndRLy/images/3859187498-app_view_nodes.png)](https://bitbucket.org/repo/zndRLy/images/3859187498-app_view_nodes.png)
[![app_view_chart.png](https://bitbucket.org/repo/zndRLy/images/2297801334-app_view_chart.png)](https://bitbucket.org/repo/zndRLy/images/2297801334-app_view_chart.png)